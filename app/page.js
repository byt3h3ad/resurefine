"use client";

import { useChat } from "ai/react";
import { useRef, useState } from "react";
import { toast } from "react-hot-toast";
import { Header, Footer } from "@/components";

export default function Home() {
  const [inp, setInp] = useState("");
  const outRef = useRef(null);

  const scrollToOut = () => {
    if (outRef.current !== null) {
      outRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  const { messages, input, handleInputChange, handleSubmit, isLoading } =
    useChat({
      body: {
        inp,
      },
      onResponse() {
        scrollToOut();
      },
    });

  const onSubmit = (e) => {
    setInp(input);
    handleSubmit(e);
  };

  const lastMessage = messages[messages.length - 1];
  const generated =
    lastMessage?.role === "assistant" ? lastMessage.content : null;
  return (
    <>
      <Header />
      <main className="flex max-w-5xl mx-auto flex-col items-center justify-center min-h-screen">
        <div className="flex flex-col w-full max-w-md py-24 mx-auto stretch">
          {messages.length > 0
            ? messages.map((m) => (
              <div key={m.id} className="whitespace-pre-wrap">
                {m.role === "user" ? "User: " : "AI: "}
                {m.content}
              </div>
            ))
            : null}
          <form onSubmit={onSubmit}>
            <input
              className="fixed bottom-12 w-full max-w-md p-2 mb-8 border border-gray-300 rounded shadow-xl focus:border-black focus:ring-black"
              value={input}
              placeholder="Enter your text..."
              onChange={handleInputChange}
            />
            {!isLoading && (
              <button
                className="bg-black rounded-xl text-white font-medium px-4 py-2 sm:mt-10 mt-8 hover:bg-black/80 w-full"
                type="submit"
              >
                Refine your Resume &rarr;
              </button>
            )}
            {isLoading && (
              <button
                className="bg-black rounded-xl text-white font-medium px-4 py-2 sm:mt-10 mt-8 hover:bg-black/80 w-full"
                disabled
              >
                <span className="loading">
                  <span style={{ backgroundColor: "white" }} />
                  <span style={{ backgroundColor: "white" }} />
                  <span style={{ backgroundColor: "white" }} />
                </span>
              </button>
            )}
          </form>
          <hr className="h-px bg-gray-700 border-1 dark:bg-gray-700" />
          <output className="space-y-10 my-10">
            {generated && (
              <>
                <div>
                  <h2
                    className="sm:text-4xl text-3xl font-bold text-slate-900 mx-auto"
                    ref={outRef}
                  >
                    Your generated texts
                  </h2>
                </div>
                <div className="space-y-8 flex flex-col items-center justify-center max-w-xl mx-auto">
                  {generated
                    .substring(generated.indexOf("1") + 3)
                    .split("2.")
                    .map((generatedBio) => {
                      return (
                        <div
                          className="bg-white rounded-xl shadow-md p-4 hover:bg-gray-100 transition cursor-copy border"
                          onClick={() => {
                            navigator.clipboard.writeText(generatedBio);
                            toast("Bio copied to clipboard", {
                              icon: "✂️",
                            });
                          }}
                          key={generatedBio}
                        >
                          <p>{generatedBio}</p>
                        </div>
                      );
                    })}
                </div>
              </>
            )}
          </output>
          <Footer />
        </div>
      </main>
    </>
  );
}
