import { Configuration, OpenAIApi } from "openai-edge";
import { OpenAIStream, StreamingTextResponse } from "ai";

export const runtime = "edge";

const config = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(config);

export async function POST(req) {
  const { messages } = await req.json();
  const response = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    stream: true,
    messages: [
      {
        role: "user",
        content: `Generate a professional text of two lines based on the context: ${messages}. The text should be crisp and professional. Clearly label them as "1." and "2.".`,
      },
    ],
  });
  //   return NextResponse.json("Chat API route");
  const stream = OpenAIStream(response);
  return new StreamingTextResponse(stream);
}
