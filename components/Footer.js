export const Footer = () => {
  return (
    <footer className="flex items-center justify-center font-mono font-bold p-5 text-[#252627] bg-[#a9a9b3] min-w-full bottom-0 fixed left-0">
      <a
        href="https://peerlist.io/adhiraj"
        rel="noreferrer"
        target="_blank"
        title="Adhiraj on Peerlist"
      >
        <span className="mr-1">{">"}</span>
        <span className="text-lg">_ bytehead</span>
        <span className="inline-block w-[10px] h-4 bg-[#fe5186] ml-1 rounded-[1px] animate-blink"></span>
      </a>
    </footer>
  );
};
