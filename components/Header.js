import Image from "next/image";
import Link from "next/link";

export const Header = () => {
  return (
    <header className="flex justify-between items-center w-full mt-5 border-b-2 pb-4 sm:px-4 px-2">
      <Link href="/" prefetch={false} className="flex space-x-3">
        <h1 className="sm:text-4xl text-2xl font-bold ml-2 tracking-tight">
          ResuRefine
        </h1>
      </Link>
      <a
        href="https://gitlab.com/byt3h3ad/resurefine"
        target="_blank"
        rel="noreferrer"
        className="text-2xl font-bold mr-2 hover:text-[#FC6D26] text-[#FCA326]"
        title="ResuRefine"
      >
        GitLab
      </a>
    </header>
  );
};
